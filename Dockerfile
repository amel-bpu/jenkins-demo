FROM ubuntu:16.04
MAINTAINER Amel Ajdinovic

ENV LANG=C.UTF-8 LC_ALL=C.UTF-8

RUN echo "Creating app folder"
RUN mkdir /app

COPY . /app
WORKDIR /app

RUN apt-get update -y
RUN apt-get install -y wget curl lsof git python-pip libmysqlclient-dev build-essential libssl-dev bzip2 \
                       ca-certificates libssl-dev libffi-dev python-dev python-pip software-properties-common

RUN echo 'export PATH=/opt/miniconda/bin:$PATH' > /etc/profile.d/conda.sh && \
    curl -o ~/miniconda.sh https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh && \
    /bin/bash ~/miniconda.sh -b -p /opt/miniconda && \
    rm ~/miniconda.sh

RUN mkdir /config

COPY requirements.yml /config
RUN /opt/miniconda/bin/conda env create -f /config/requirements.yml
ENV PATH /opt/miniconda/envs/bpu-datacollector/bin:$PATH

RUN /opt/miniconda/bin/conda update -n base -c defaults conda

ENTRYPOINT ["python"]
CMD ["run.py"]